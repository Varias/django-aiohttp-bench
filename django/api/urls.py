from django.conf.urls import url, include

from .views import BlogPostCreateView, BlogPostListView, BlogPostRetrieveView
from .helpers import method_dispatch


posts_urlpatterns = [
    url(r'^$', method_dispatch(
        POST=BlogPostCreateView.as_view(),
        GET=BlogPostListView.as_view()
    )),
    url(r'^(?P<pk>[\d]+)$', BlogPostRetrieveView.as_view())
]


urlpatterns = [
    url(r'^posts/', include(posts_urlpatterns, namespace='posts')),
]
