from django.db import models
from django.utils.translation import ugettext_lazy as _


class BlogPost(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name=_('Title'))
    text = models.TextField(verbose_name=_('Text'))
    published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Blog post #{}'.format(self.pk)

    class Meta:
        verbose_name = 'Blog post'
        verbose_name_plural = 'Blog posts'
