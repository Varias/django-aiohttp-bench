
Установка
---------

Скачивание образов, сборка::

    docker-compose pull
    docker-compose build


Скопировать файлы конфигов, при необходимости поменять настройки::

    # Postgres
    cp dockerfiles/postgres/postgresql.conf.example dockerfiles/postgres/postgresql.conf
    cp dockerfiles/postgres/private.env.example dockerfiles/postgres/private.env
    cp dockerfiles/postgres/public.env.example dockerfiles/postgres/public.env

    # aiohttp
    cp dockerfiles/aiohttp/env.example dockerfiles/aiohttp/env
    cp dockerfiles/aiohttp/gunicorn.py.example dockerfiles/aiohttp/gunicorn.py

    # Django
    cp dockerfiles/django/env.example dockerfiles/django/env
    cp dockerfiles/django/gunicorn.py.example dockerfiles/django/gunicorn.py


Далее нужно запустить контейнер с Postgres, дождаться создания БД, погасить контейнер::

    docker-compose up postgres

Для тестов следует установить утилиту wrk (https://github.com/wg/wrk)

Запуск для aiohttp::

    docker-compose up aiohttp


Запуск для Django::

    docker-compose up django


Тесты для aiohttp::

    wrk -t1 -c25 -d30s -s requests/posts/create.lua http://localhost:8001/api/posts

Тесты для Django::

    wrk -t1 -c25 -d30s -s requests/posts/create.lua http://localhost:8000/api/posts
