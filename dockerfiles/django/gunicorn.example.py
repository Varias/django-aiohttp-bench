bind = "0.0.0.0:8000"
workers = 2
worker_class = "gevent"


def post_fork(server, worker):
    from psycogreen.gevent import patch_psycopg
    patch_psycopg()
    worker.log.info("Made Psycopg2 Green")
